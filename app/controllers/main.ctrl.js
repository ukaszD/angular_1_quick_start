(function(app) {
    app.controller('mainCtrl', ['$scope', function($scope) {
        "use strict";

        $scope.test = 'test';

        $scope.tabs = [{
            "title": "MAIN_PAGE",
            "routeLink": "main_page"
        },
        {
            "title": "DETAILS",
            "routeLink": "details"
        }];
    }]);
}(angular.module('app')));
