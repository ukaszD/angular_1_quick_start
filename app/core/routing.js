(function(app) {
    "use strict";
    app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider.state('main_page', {
            url: "/",
            templateUrl: "templates/main_page.tmpl.html"

        }).state('details', {
            url: "/details/:itemId",
            templateUrl: 'templates/details.tmpl.html',
            controller: function($scope, $stateParams) {
                $scope.itemId = $stateParams.itemId;
            }
        });
    }]);
}(angular.module('app')));
