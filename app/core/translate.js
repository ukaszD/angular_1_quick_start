(function(app) {
    "use strict";
    app.config(['$translateProvider', function($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'languages/',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('pl_PL');
        $translateProvider.useSanitizeValueStrategy('escape');
    }]);
}(angular.module('app')));
