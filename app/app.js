(function() {
    angular.module('app', ['ui.router', 'pascalprecht.translate', 'angular-loading-bar', 'ngAnimate', 'ngFileUpload']);
}());
