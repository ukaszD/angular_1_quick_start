var gulp = require('gulp');
var wiredep = require('wiredep').stream;
var $ = require('gulp-load-plugins')({
    lazy: true
});
var browserSync = require('browser-sync').create();

var path = {
    devIndex: 'app/index.html',
    mainApp: 'app/app.js',
    angularCoreFiles: 'app/core/**/*.js',
    angularCtrlFiles: 'app/controllers/**/*.js',
    languagesFiles: 'app/languages/**/*.json',
    templatesFiles: 'app/templates/**/*.html',
    lessStyleFiles: 'app/resources/styles/**/*.less',
    images: 'app/resources/images/**/*.*',
    fonts: 'app/resources/fonts/*.{eot,svg,ttf,woff,woff2}',
    compiledLessFiles: 'app/resources/styles/**/*.css',
    bowerComponents: './bower_components/',
    bowerJson: './bower.json',
    // publicIndex: './public/index.html',
    // publicJsFiles: './public/js/*.js',
    // publicCssFiles: './public/css/*.css'
};

gulp.task('compileLess', function() {
    return gulp.src([path.lessStyleFiles])
        .pipe($.if(/[.]less/, $.less()))
        .pipe(gulp.dest('app/resources/styles'))
        .pipe(browserSync.stream());
});

gulp.task('injectComponents', ['compileLess'], function() {
    var target = gulp.src(path.devIndex);
    var sources = gulp.src([path.mainApp, path.angularCoreFiles, path.angularCtrlFiles, path.compiledLessFiles]);
    return target.pipe($.inject(sources, {
            ignorePath: 'app/'
        }))
        .pipe(gulp.dest('app/'));
});

gulp.task('wiredepComponents', function() {
    gulp.src(path.devIndex)
        .pipe(wiredep({
            directory: path.bowerComponents,
            ignorePath: '../../',
            fileTypes: {
                html: {
                    replace: {
                        js: '<script src="{{filePath}}"></script>',
                        css: '<link rel="stylesheet" href="{{filePath}}"/>'
                    }
                }
            }
        }))
        .pipe(gulp.dest('app/'));
});

// //------------------- publish tasks --------------------------------------------
//
// gulp.task('publish_includeComponents', function () {
//     return gulp.src(path.devIndex)
//         .pipe($.useref())
//         .pipe($.ignore.exclude([ "**/*.map" ]))
//         .pipe($.if(['**/*.js', '!**/*.min.js'], $.uglify({compress: false}).on('error', function(e) { console.log('\x07',e.message); return this.end(); })))
//         .pipe($.if(['**/*.css'], $.uglifycss()))
//         .pipe(gulp.dest('./public/'));
// });
//
// gulp.task('publish_includeViews', function () {
//     return gulp.src(path.views)
//         .pipe(gulp.dest('public/views'));
// });
//
// gulp.task('publish_includeLanguages', function () {
//     return gulp.src(path.languagesFiles)
//         .pipe(gulp.dest('public/languages'));
// });
//
// gulp.task('publish_includeImages', function () {
//     return gulp.src(path.images)
//         .pipe(gulp.dest('public/resources/img'));
// });
//
// gulp.task('publish_includeFonts', function () {
//     return gulp.src(path.fonts)
//         .pipe(gulp.dest('public/fonts/'));
// });
//
// gulp.task('publish', ['publish_includeComponents', 'publish_includeViews', 'publish_includeImages', 'publish_includeFonts','publish_includeLanguages'], function () {
// });
//
// //------------------------------------------------------------------------------

gulp.task('default', ['wiredepComponents', 'injectComponents'], function() {});

gulp.task('browser-sync', ['default'], function() {
    browserSync.init({
        server: {
            baseDir: ['./app', './bower_components'],
            browser: "google chrome",
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });
    gulp.watch(path.lessStyleFiles, ['injectComponents']);
    gulp.watch([path.templatesFiles,path.devIndex]).on('change', browserSync.reload);
    gulp.watch([path.mainApp,path.angularCoreFiles, path.angularCtrlFiles]).on('change', browserSync.reload);
    gulp.watch([path.languagesFiles]).on('change', browserSync.reload);
});
